import React from 'react';
import { Route } from 'react-router-dom';
import Home from '../../pages/Home/Home';
import Menu from '../../pages/Menu/Menu';
import FAQ from '../../pages/FAQ/FAQ';
import ContactUs from '../../pages/ContactUs/ContactUs';
import Reservations from '../../pages/Reservations/Reservations';
import UpcomingEvents from '../../pages/UpcomingEvents/UpcomingEvents';

class Main extends React.Component {
   render() {
        return (
            <main id="Main">
                <Route exact path='(/default.asp)?' component={Home} data="Home"/>
                <Route path="/menu/" component={Menu} data="Menu"/>
                <Route path="/reservations/" component={Reservations} page="Reservations"/>
                <Route path="/faq/" component={FAQ} page="FAQ"/>
                <Route path="/contact-us/" component={ContactUs} page="ContactUs"/>
                <Route path="/upcoming-events/" component={UpcomingEvents} page="UpcomingEvents"/>
            </main>
        );
    }
}

export default Main;
