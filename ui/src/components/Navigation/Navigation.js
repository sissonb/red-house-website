import React from 'react';
import {Link} from 'react-router-dom';
import {withRouter} from 'react-router-dom';

class Navigation extends React.Component {
    toggleMenu() {
        this.refs.menuList.classList.toggle("is-active");
        this.refs.menuBurger.classList.toggle("is-active");
    }

    closeMenu() {
        this.refs.menuList.classList.remove("is-active");
        this.refs.menuBurger.classList.remove("is-active");
    }

    render() {
        const pathname = this.props.location.pathname;
        return (
            <nav id="Navigation">
                <ul ref="menuList">
                    <li className={(pathname === '/') ? 'active' : '' }>
                        <Link to="/" onClick={this.closeMenu.bind(this)}>Home</Link>
                    </li>
                    <li className={(pathname === '/menu/') ? 'active' : '' }>
                        <Link to="/menu/" onClick={this.closeMenu.bind(this)}>Menu</Link>
                    </li>
                    <li className={(pathname === '/reservations/') ? 'active' : '' }>
                        <Link to="/reservations/" onClick={this.closeMenu.bind(this)}>Reservations</Link>
                    </li>
                    <li className={(pathname === '/faq/') ? 'active' : '' }>
                        <Link to="/faq/" onClick={this.closeMenu.bind(this)}>FAQ</Link>
                    </li>
                    <li className={(pathname === '/contact-us/') ? 'active' : '' }>
                        <Link to="/contact-us/" onClick={this.closeMenu.bind(this)}>Contact Us</Link>
                    </li>
                    <li className={(pathname === '/upcoming-events/') ? 'active' : '' }>
                        <Link to="/upcoming-events/" onClick={this.closeMenu.bind(this)}>Upcoming Events</Link>
                    </li>
                </ul>
                <div ref="menuBurger" className="hamburger hamburger--spin" onClick={this.toggleMenu.bind(this)}>
                    <div className="hamburger-box">
                        <div className="hamburger-inner"/>
                    </div>
                </div>
            </nav>
        );
    }
}

export default withRouter(Navigation);