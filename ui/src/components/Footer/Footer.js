import React from 'react';
import outboundTracking from '../../utility/outboundTracking';

class Footer extends React.Component {
    constructor() {
        super();
        this.state = {
            data: {
                result: {
                    opening_hours: {
                        open_now: '',
                        weekday_text: [],
                    },
                },
            },
        };
    }

    copyGoogleDataToState(data) {
        this.setState({ data });
    }

    render() {
        const weekdayTextIndex = (new Date().getDay() + 6) % 7;
        return (
            <div id="Footer">
                <div className="container">
                    <div>
                        <h3>Call Us</h3>
                        <p className="call-us"><a onClick={outboundTracking.bind(this, 'Telephone')}
                                                  href="tel:4252262666">(425) 226-2666</a></p>
                        <h3>Email Us</h3>
                        <p className="email-us">
                            <a onClick={outboundTracking.bind(this, 'Email')}
                               href="mailto:contactus@redhouserenton.com">contactus@redhouserenton.com</a>
                        </p>
                    </div>
                    <div>
                        <h3>Directions</h3>
                        <address>
                            <p className="directions">
                                <a onClick={outboundTracking.bind(this, 'Directions')}
                                   href="https://www.google.com/maps/dir/My+Location/Red+House+Beer+%26+Wine+Shoppe,+410+Burnett+Ave+S,+Renton,+WA+98057">
                                    410 Burnett Ave S, Renton, WA 98057
                                </a>
                            </p>
                        </address>
                    </div>
                    <div>
                        <h3>Restaurant Hours</h3>
                        <ul className="restaurant-hours">
                            {this.state.data.result.opening_hours.weekday_text.map((dayHours, index) => {
                                if (weekdayTextIndex === index) {
                                    return <li key={dayHours}><b>{dayHours}</b></li>;
                                } else {
                                    return <li key={dayHours}>{dayHours}</li>;
                                }
                            })}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;