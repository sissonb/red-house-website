const outboundTracking = (eventLabel) => {
    if (typeof window.ga === 'function') {
        window.ga('send', {
            hitType: 'event',
            eventCategory: 'outbound',
            eventAction: 'click',
            eventLabel: eventLabel,
            transport: 'beacon',
        });
    }
};

export default outboundTracking;