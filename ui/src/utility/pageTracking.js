const pageTracking = (page) => {
    if (typeof window.ga === 'function') {
        window.ga('set', 'page', page);
        window.ga('send', 'pageview');
    }
};

export default pageTracking;