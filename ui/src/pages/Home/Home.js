import React, { Component } from 'react';
import pageTracking from '../../utility/pageTracking';
import { createHashHistory } from 'history';

class Home extends Component {
    componentDidMount() {
        if (window.location.pathname === '/default.asp') {
            createHashHistory();
            this.props.history.push('/');
        }
        pageTracking('Home');
    }

    render() {
        const openedDate = new Date(2004, 8, 29);
        const millisecondsSinceOpened = new Date().getTime() - openedDate.getTime();
        const closenessToOpenDate = millisecondsSinceOpened % 31556952000 / 31556952000;

        // 31556952000 ms in a year
        const years = Math.floor(millisecondsSinceOpened / 31556952000);
        let introText;
        if (closenessToOpenDate < .2) {
            introText = 'A little more than ' + years;
        } else {
            introText = 'More than ' + years;
        }

        return (
            <article id="HomePage">
                <h2>Welcome to The Red House</h2>
                <p className="align-center"><img alt="Red House Logo" src="/img/red-house-logo.png" /></p>
                <p>{introText} years ago my wife and I created The Red House in a former 1920's boarding
                    house home to railroad workers and coal miners.</p>
                <p>The comfortable earthy vibe remains, we simply added an excellent selection of beer and wine. We
                    are a restaurant and a beer and wine shoppe. Our food pleases young and old alike yet still
                    excites the senses. The steady hand of our professional staff delivers service that adds the
                    finishing touch to what's become The Red House Experience.</p>
                <p>Welcome to the Red House, and please enjoy!</p>
                <p className="signed">Gene</p>
            </article>
        );
    }
}

export default Home;
