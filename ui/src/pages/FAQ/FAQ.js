import React, { Component } from 'react';
import pageTracking from '../../utility/pageTracking';

class FAQ extends Component {
    componentDidMount() {
        pageTracking('FAQ');
    }

    render() {
        return (
            <article id="FAQPage">
                <h2>FAQ</h2>
                <dl>
                    <dt>
                        <h3>Do you sell Liquor?</h3>
                    </dt>
                    <dd>
                        <p>No we don't carry any Liquor, but we do carry an amazing selection of beer and wine</p>
                    </dd>

                    <dt>
                        <h3>Do you allow kids?</h3>
                    </dt>
                    <dd>
                        <p>Yes, we offer kid size portions of some of our regular dishes and can make a few things that
                            are not on the menu</p>
                    </dd>
                    <dt>
                        <h3>Do you have a gluten-free menu?</h3>
                    </dt>
                    <dd>
                        <p>We have several items on our menu that are gluten-free and several that can be made
                            gluten-free.</p>
                    </dd>
                    <dt>
                        <h3>Do you sell Kegs or Growlers?</h3>
                    </dt>
                    <dd>
                        <p>We do not sell kegs or growlers, but we do fill growlers.</p>
                    </dd>
                    <dt>
                        <h3>What is the maximum size party I can book at the Red House?</h3>
                    </dt>
                    <dd>
                        <p>We have rooms up stairs that can hold different sized parties. Our entire upstairs can hold a
                            maximum of 50 people.</p>
                    </dd>
                    <dt>
                        <h3>Do you take special orders for beer or wine?</h3>
                    </dt>
                    <dd>
                        <p>Yes, and if you buy a full case we will give you an additional 10% off the case price.</p>
                    </dd>
                </dl>
            </article>
        );
    }
}

export default FAQ;