import React, { Component } from 'react';
import outboundTracking from '../../utility/outboundTracking';
import pageTracking from '../../utility/pageTracking';

class Reservations extends Component {
    componentDidMount() {
        pageTracking('Reservations');
    }

    render() {
        return (
            <article id="ReservationsPage">
                <h2>Reservations</h2>
                <div className="align-center">
                    <a onClick={outboundTracking.bind(this, 'Open Table')}
                       href="https://www.opentable.com/r/red-house-reservations-renton?restref=61378&lang=en">
                        <img className="open-table" src="/img/open-table.png" alt="opentable icon"/>
                        <p>Reserve online using OpenTable</p>
                    </a>
                </div>
                <p>For parties over 10 please give us a call at <a onClick={outboundTracking.bind(this, 'Telephone')} href="tel:4252262666">(425) 226-2666</a></p>
                <p>*We do not hold patio tables for reservations, but we do give priority to requests</p>
            </article>
        );
    }
}
export default Reservations;
