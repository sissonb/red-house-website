import React, { Component } from 'react';
import outboundTracking from '../../utility/outboundTracking';
import pageTracking from '../../utility/pageTracking';

class Menu extends Component {
    componentDidMount() {
        pageTracking('Menu');
    }

    render() {
        return (
            <article id="MenuPage">
                <h2>Menu</h2>
                <p>In addition to great food, the Red House features an extensive selection of wines and beers
                    available for purchase – choose from hundreds of varieties from around the world.</p>
                <p>Our knowledgeable staff is always available to recommend a perfect bottle for any
                    occasion.</p>
                <p>Prices and availability subject to change</p>
                <p className="align-center" style={{
                    margin: 10,
                }}>
                    <a onClick={outboundTracking.bind(this, 'Menu')}
                       href="https://drive.google.com/file/d/0B8zWSghkNN83SUR6RE1IVUE2QWc/view">
                        Click here to view the menu in fullscreen
                    </a>
                </p>
                <p className="align-center" style={{
                    margin: 10,
                }}>
                    <a onClick={outboundTracking.bind(this, 'Menu')}
                       href="https://drive.google.com/uc?authuser=0&id=0B8zWSghkNN83SUR6RE1IVUE2QWc&export=download">
                        Click here to download the menu
                    </a>
                </p>
                <div className="iframe-container">
                    <iframe className="iframe"
                            title="Red House Menu"
                            src="https://drive.google.com/file/d/0B8zWSghkNN83SUR6RE1IVUE2QWc/preview"/>
                </div>
            </article>
        );
    }
}

export default Menu;