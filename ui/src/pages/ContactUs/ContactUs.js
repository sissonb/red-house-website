import React, {Component} from 'react';
import pageTracking from '../../utility/pageTracking';

class ContactUs extends Component {
    componentDidMount() {
        pageTracking('ContactUs');
    }
    render() {
        return (
            <article id="ContactUsPage">
                <h2>Contact Us</h2>
                <p>Have a question, comment, or need more info about large partys?
                    We would love to hear from you!</p>
                <h2><a href="tel:4252262666">(425) 226-2666</a></h2>
            </article>
        );
    }
}
export default ContactUs;
