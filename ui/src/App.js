import axios from 'axios';
import React from 'react'

import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Main from './components/Main/Main';
import Navigation from './components/Navigation/Navigation';
import SocialLinks from './components/SocialLinks/SocialLinks';

class App extends React.Component {
    componentDidMount() {

        const thisRef = this;
        axios.get('/services/google-places-info/')
            .then(function (response) {
                thisRef.refs.footer.copyGoogleDataToState(response.data);
                thisRef.refs.header.copyGoogleDataToState(response.data);
            })
            .catch(function (error) {
                const data = {
                    "html_attributions": [],
                    "result": {
                        "address_components": [
                            {
                                "long_name": "410",
                                "short_name": "410",
                                "types": ["street_number"]
                            },
                            {
                                "long_name": "Burnett Avenue South",
                                "short_name": "Burnett Ave S",
                                "types": ["route"]
                            },
                            {
                                "long_name": "Downtown Renton",
                                "short_name": "DTR",
                                "types": ["neighborhood", "political"]
                            },
                            {
                                "long_name": "Renton",
                                "short_name": "Renton",
                                "types": ["locality", "political"]
                            },
                            {
                                "long_name": "King County",
                                "short_name": "King County",
                                "types": ["administrative_area_level_2", "political"]
                            },
                            {
                                "long_name": "Washington",
                                "short_name": "WA",
                                "types": ["administrative_area_level_1", "political"]
                            },
                            {
                                "long_name": "United States",
                                "short_name": "US",
                                "types": ["country", "political"]
                            },
                            {
                                "long_name": "98057",
                                "short_name": "98057",
                                "types": ["postal_code"]
                            },
                            {
                                "long_name": "7501",
                                "short_name": "7501",
                                "types": ["postal_code_suffix"]
                            }
                        ],
                        "adr_address": "\u003cspan class=\"street-address\"\u003e410 Burnett Ave S\u003c/span\u003e, \u003cspan class=\"locality\"\u003eRenton\u003c/span\u003e, \u003cspan class=\"region\"\u003eWA\u003c/span\u003e \u003cspan class=\"postal-code\"\u003e98057-7501\u003c/span\u003e, \u003cspan class=\"country-name\"\u003eUSA\u003c/span\u003e",
                        "formatted_address": "410 Burnett Ave S, Renton, WA 98057, USA",
                        "formatted_phone_number": "(425) 226-2666",
                        "geometry": {
                            "location": {
                                "lat": 47.47775009999999,
                                "lng": -122.2075898
                            },
                            "viewport": {
                                "northeast": {
                                    "lat": 47.47910173029149,
                                    "lng": -122.2063666197085
                                },
                                "southwest": {
                                    "lat": 47.47640376970849,
                                    "lng": -122.2090645802915
                                }
                            }
                        },
                        "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                        "id": "5be7872aca09ac2299e7f2dba37121db94d22793",
                        "international_phone_number": "+1 425-226-2666",
                        "name": "Red House Beer & Wine Shoppe",
                        "opening_hours": {
                            "open_now": false,
                            "periods": [
                                {
                                    "close": {
                                        "day": 0,
                                        "time": "2100"
                                    },
                                    "open": {
                                        "day": 0,
                                        "time": "1100"
                                    }
                                },
                                {
                                    "close": {
                                        "day": 1,
                                        "time": "2100"
                                    },
                                    "open": {
                                        "day": 1,
                                        "time": "1100"
                                    }
                                },
                                {
                                    "close": {
                                        "day": 2,
                                        "time": "2100"
                                    },
                                    "open": {
                                        "day": 2,
                                        "time": "1100"
                                    }
                                },
                                {
                                    "close": {
                                        "day": 3,
                                        "time": "2100"
                                    },
                                    "open": {
                                        "day": 3,
                                        "time": "1100"
                                    }
                                },
                                {
                                    "close": {
                                        "day": 4,
                                        "time": "2200"
                                    },
                                    "open": {
                                        "day": 4,
                                        "time": "1100"
                                    }
                                },
                                {
                                    "close": {
                                        "day": 5,
                                        "time": "2200"
                                    },
                                    "open": {
                                        "day": 5,
                                        "time": "1100"
                                    }
                                },
                                {
                                    "close": {
                                        "day": 6,
                                        "time": "2200"
                                    },
                                    "open": {
                                        "day": 6,
                                        "time": "1100"
                                    }
                                }
                            ],
                            "weekday_text": [
                                "Monday: 11:00 AM – 9:00 PM",
                                "Tuesday: 11:00 AM – 9:00 PM",
                                "Wednesday: 11:00 AM – 9:00 PM",
                                "Thursday: 11:00 AM – 10:00 PM",
                                "Friday: 11:00 AM – 10:00 PM",
                                "Saturday: 11:00 AM – 10:00 PM",
                                "Sunday: 11:00 AM – 9:00 PM"
                            ]
                        },
                        "photos": [
                            {
                                "height": 2988,
                                "html_attributions": [
                                    "\u003ca href=\"https://maps.google.com/maps/contrib/104763639736725633505/photos\"\u003eDmitriy Todd\u003c/a\u003e"
                                ],
                                "photo_reference": "CmRaAAAAof7nUT5Smy5d0LVGwU1G12mg_KCCyizmtIt635jVc5khzDrO3oqua8y1HX5v8dbLD058kIFToKksVfPC7xQm89IDFZmF3DCrsuoWnQqSjnpiX0K9ABCX23GR-Rscg6l1EhAKLw-fa4VxhdV9I76yU1mDGhTPMGnbXRNsgvezHkJZEU_kMhKUbA",
                                "width": 5312
                            },
                            {
                                "height": 2988,
                                "html_attributions": [
                                    "\u003ca href=\"https://maps.google.com/maps/contrib/118433659701788556614/photos\"\u003eDoug Cisler\u003c/a\u003e"
                                ],
                                "photo_reference": "CmRZAAAAGFZjdIkkehJKajGeZ89cfvkI0PhSrFs_KeZv_Uv-h9AXiS09OY8Nrg6jft5G9xBW-fpmHCYC_eL-l190cuQjfRfNmYQwCiRardsdvs-ENg28Jfjg7crmWVViR0TMF0-CEhDaVqZlO7JDFd7jIgm05o1dGhTfEts1lx0P2HQ1duBnNsQY0IW1hA",
                                "width": 5312
                            },
                            {
                                "height": 5312,
                                "html_attributions": [
                                    "\u003ca href=\"https://maps.google.com/maps/contrib/101503329212225924339/photos\"\u003eKurt Nicholas\u003c/a\u003e"
                                ],
                                "photo_reference": "CmRaAAAAPhzB_SgiqRYbkq4tvvYouA-yU6pUGCEbxhrDcomoV9L42Uc0D9thbvOlmPzeUwgR8wqGmjNwdYxs1i3uWGc5lOycgKbyD1nXMqQ4txcTBSK7rXbbQkim28jqRBQAhITCEhA44E45J4ThfYT4jHIZb190GhQmJhoflqBNsEAVy4TpTwcnLp9YRg",
                                "width": 2988
                            },
                            {
                                "height": 3120,
                                "html_attributions": [
                                    "\u003ca href=\"https://maps.google.com/maps/contrib/111253366281338190509/photos\"\u003eRyo Canda\u003c/a\u003e"
                                ],
                                "photo_reference": "CmRaAAAAiIl8h7MR8ARXh0DP7jSGDBF87K5ZGjnUbF1kYSmUAp1w-xBHkmLXwVLY6kTqbbwTvdEpSKKr6yNWTp_K7Mlr4jp7azEn1UMJu892l6ahwlgD8yi8y_Qlz9V9gXUT1xZAEhDZWyUJrbcbjhtt2ugeY0BpGhT8ekGcAR_d8vx4RxzCz0OJD34wCw",
                                "width": 4160
                            },
                            {
                                "height": 3024,
                                "html_attributions": [
                                    "\u003ca href=\"https://maps.google.com/maps/contrib/114153121614252022083/photos\"\u003eRoger Hanson\u003c/a\u003e"
                                ],
                                "photo_reference": "CmRaAAAAqMncp8-Y6_-dMq-tQ7QSeGEQav8YHFxFLxTWrAa9-xUDaEEA7TGjerN5S5JKRkE9uYqQcp92t4f2YbTbob_rJ4UCEztHRYeGAbElubvixMELf-RIfa1wmvvXal-IYcH5EhBaP2MbRGV4WafhldF_1azpGhR6oz9JYS36XCb01GCB6GoTb1VEDA",
                                "width": 4032
                            },
                            {
                                "height": 4032,
                                "html_attributions": [
                                    "\u003ca href=\"https://maps.google.com/maps/contrib/110990218951216891983/photos\"\u003eMichael Hokett\u003c/a\u003e"
                                ],
                                "photo_reference": "CmRaAAAA27g7D76mhfblq435fUR46zNRUvDbFePtuM3eMl2n9bTvVcSILfF1EeLq1MDKsqxUdANR2CQZSEZYVZ3yOeAfYV_oeryvPHmx27ksEcJr1FXvFAder5JovBW92hY4q-q-EhCAeDaAs5c_qiF1-F7MGttjGhQz7AWbdJ0KQlx_zloNLBFU0tDg5g",
                                "width": 3024
                            },
                            {
                                "height": 3024,
                                "html_attributions": [
                                    "\u003ca href=\"https://maps.google.com/maps/contrib/106536197678259425079/photos\"\u003eBill Weber\u003c/a\u003e"
                                ],
                                "photo_reference": "CmRaAAAAC32xOSJJj6H9B_6-KxEnS0lXYDG7SRrUN8eycDeLsOF6cq3HaUExXEqw_YDTzV1vjBxTXqQPlqqdq0h6U1O2tFnVP70tKLC2Xr2jcUQ-mS3Z4wd8TuqS027i70XiEwr5EhCRl62Xj1TU48-54pQn99T0GhRuFiqGL7LMulU1bKqRTwUxqS_rlA",
                                "width": 4032
                            },
                            {
                                "height": 4032,
                                "html_attributions": [
                                    "\u003ca href=\"https://maps.google.com/maps/contrib/106536197678259425079/photos\"\u003eBill Weber\u003c/a\u003e"
                                ],
                                "photo_reference": "CmRaAAAAVVP-V_z0y8ZlpvQut5OocjEUxj5k99XqKlalaEpOUDEvyoMb-_hfYZ058DXmoy57rErX4Y94han1IF8pp-hre69hNsyIrurLv3hYK8HPJ07gQRA2KfaX8hEjqPkAmZ03EhAnsKZJzwoamAbRqa0Tj-o6GhS_G4LAqpxM77z6YG0r-G5eszXN0w",
                                "width": 3024
                            },
                            {
                                "height": 4032,
                                "html_attributions": [
                                    "\u003ca href=\"https://maps.google.com/maps/contrib/114240636432713845723/photos\"\u003eMiguel Eduardo Mármol\u003c/a\u003e"
                                ],
                                "photo_reference": "CmRaAAAA1MADJM6ErQuRdmKRwYgIoPcx__eR79tMYc-Bpv5bPA4j3ik7U5VMV7ehE5HHPr3Tu5HWSi35t6lzGfPjW0WsFFJNFAhcUC-nJY8E7pPGMlTBz9xkJVmTatYT-_a00BUkEhBcmvUyep4BTS-tE5z9in91GhTWMEKrakveAlMNYDQJNk5nHF2hNA",
                                "width": 3024
                            },
                            {
                                "height": 2988,
                                "html_attributions": [
                                    "\u003ca href=\"https://maps.google.com/maps/contrib/118433659701788556614/photos\"\u003eDoug Cisler\u003c/a\u003e"
                                ],
                                "photo_reference": "CmRZAAAAgn-gs_eMJWh3_WQ-FGx35mcAyDAV-XpWmLaJDthhUcVSG9QmPnspWJYEr6vnp6goM7x5-r_dxRzsCWGjCul3QvNgQzsx5lr0oFJ5Y1mKuJ1oJjdj1nsnAOA--JFbHsGqEhBhryqjKWjzsryIBY-VLO5NGhTsYOLWLfcMhs4O7lai89yPo8y2GA",
                                "width": 5312
                            }
                        ],
                        "place_id": "ChIJ0f3UTVNdkFQR-qomtr-RMDw",
                        "rating": 4.4,
                        "reference": "CmRRAAAA0fRJpmVhmj5zPlq7sLIdbxD78rz_1gKc1WyXy7Q4r1lbsHkm0-1VpaCtMqMeMq7eb_peidH3ABcZlkCQKFfqw_0YhpP5AoK2OgIFEy9fONpJbkXUMZCuZHM-QhIgRa_DEhCVXj8jtmANIfQmWZGVXZXUGhSLu2x44APol7YJxF9Ybhj_Vk7iaQ",
                        "reviews": [
                            {
                                "author_name": "Amber Skidmore",
                                "author_url": "https://www.google.com/maps/contrib/107680771169524367235/reviews",
                                "language": "en",
                                "profile_photo_url": "https://lh3.googleusercontent.com/--jllLs5nYbg/AAAAAAAAAAI/AAAAAAAAAAA/AI6yGXybG8xv337bHr_uWLUb7V8wbhnr9A/s128-c0x00000000-cc-rp-mo-ba3/photo.jpg",
                                "rating": 4,
                                "relative_time_description": "3 months ago",
                                "text": "Stopped by for lunch one Saturday while in Renton running errands. Delicious food and nice atmosphere. I had the tomato with goat cheese soup (a tangy bisque), crab cake appetizer and Cesar salad. \nNo one seemed upset that we had a 2 year old with us even though we were the only table with a child.\nOnly complaint was the chairs weren't that comfortable, but maybe they did that on purpose so people don't linger. :) \nI would definitely eat here again if I were in the area.",
                                "time": 1491367480
                            },
                            {
                                "author_name": "Stephen Snider",
                                "author_url": "https://www.google.com/maps/contrib/111897017200355887156/reviews",
                                "language": "en",
                                "profile_photo_url": "https://lh4.googleusercontent.com/-f9iDVuzOWbI/AAAAAAAAAAI/AAAAAAAAEkQ/gbvopHZ0aTs/s128-c0x00000000-cc-rp-mo-ba3/photo.jpg",
                                "rating": 5,
                                "relative_time_description": "3 weeks ago",
                                "text": "We visited late afternoon for a drink and light food. The mushrooms and falafel were superb. Wine selection was almost overwhelming. Sangria was sweet, but refreshing, and the beer was good too. Service was exceptional, and the deck seating is the way to go. ",
                                "time": 1498144310
                            },
                            {
                                "author_name": "Melissa Richards",
                                "author_url": "https://www.google.com/maps/contrib/107937513036145925895/reviews",
                                "language": "en",
                                "profile_photo_url": "https://lh3.googleusercontent.com/-NjHWwta163c/AAAAAAAAAAI/AAAAAAAAAAA/AI6yGXx6nFJFwvoKizYVLtXkyUiJZ-w0vg/s128-c0x00000000-cc-rp-mo/photo.jpg",
                                "rating": 2,
                                "relative_time_description": "3 weeks ago",
                                "text": "We keep giving this place another try but for the $, not worth it.\nMy husband had the vegetarian shepard pie, which is basically mashed potatoes and brown gravy with vegetables in it for $15.  I had the pork shoulder dish with brussels sprouts for $17, which they changed since last time I ordered it (no longer comes on bed of yam-mash) and had as much fat as meat and was rather a dry dish.  My meat was greatly improved by the addition of the excess gravy from my husbands meal.  I ordered a $10 Sangria and it was so sweet I could not drink it.",
                                "time": 1498062375
                            },
                            {
                                "author_name": "I Berg",
                                "author_url": "https://www.google.com/maps/contrib/118184952348013351389/reviews",
                                "language": "en",
                                "profile_photo_url": "https://lh4.googleusercontent.com/-2etdpZEMd3E/AAAAAAAAAAI/AAAAAAAAAAA/AI6yGXxmXR_lZgZhtyb2WH_nr8GyMTr_fg/s128-c0x00000000-cc-rp-mo/photo.jpg",
                                "rating": 5,
                                "relative_time_description": "in the last week",
                                "text": "Great food and lots of it for the price. Service was quick and through. Definitely a must try!",
                                "time": 1500171349
                            },
                            {
                                "author_name": "Judith Rivas",
                                "author_url": "https://www.google.com/maps/contrib/116010864449006092159/reviews",
                                "language": "en",
                                "profile_photo_url": "https://lh4.googleusercontent.com/-DMSaujdVJvM/AAAAAAAAAAI/AAAAAAAAAAA/AI6yGXwR_UawQjfJRYsTioGiXKhbRUzXSQ/s128-c0x00000000-cc-rp-mo-ba3/photo.jpg",
                                "rating": 5,
                                "relative_time_description": "a month ago",
                                "text": "They had delicious food and great service. I tried the shakshuka, a dish I hadn't had before, and it was great!! I am definitely returning. :)",
                                "time": 1497147456
                            }
                        ],
                        "scope": "GOOGLE",
                        "types": ["restaurant", "food", "point_of_interest", "establishment"],
                        "url": "https://maps.google.com/?cid=4337126693738556154",
                        "utc_offset": -420,
                        "vicinity": "410 Burnett Avenue South, Renton",
                        "website": "http://www.redhousebeerandwine.com/"
                    },
                    "status": "OK"
                };
                thisRef.refs.footer.copyGoogleDataToState(data);
                thisRef.refs.header.copyGoogleDataToState(data);
                console.log(error);
            });
    }

    render() {
        document.body.style.minHeight = "100vh";
        return (
            <div id="App">
                <Navigation/>
                <Header ref="header"/>
                <Main/>
                <Footer ref="footer"/>
                <SocialLinks/>
            </div>
        )
    }
}
export default App;