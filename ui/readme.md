# Red House Website Source Code

## Environment Setup
Follow these instruction before building the website files  

### Dependencies
Install dependencies

#### Node.js
This project uses node.js's package manager\(npm\) to download dependencies. Either Version will work. 6.X or 8.X

https://nodejs.org/en/

#### Sass
Sass is used to build the CSS for this project. Sass is a Ruby package so it requires Ruby to be installed. 
Macs come with Ruby pre-installed. Windows will need to [download](https://rubyinstaller.org/) and install 
it. Once Ruby is installed, run `gem install sass` from the command line

## Building the website
1. Open the command prompt and navigate to the `/ui/` folder
1. Run the command `npm run build`
1. Website files will be outputted to `/ui/build/`
1. Copy these files to the `/src/red-house-website/web/` folder
    * Copy the files in the folder, not the `/ui/build/` folder itself
1. Follow directions in the root of this project to deploy

## Running the dev server on your computer
1. Open the command prompt and navigate to the `/ui/` folder
1. Run the command `npm start`
1. The browser will automatically open the website running locally on your computer
    * Making changes to the JavaScript files will automatically update the website running locally on your computer

## Making CSS changes
The `/ui/sass/` folder has the CSS styles in Sass form. The Sass will be built when either the `npm run build` or  
`npm start` command is executed from the `/ui/` folder. You can directly build the Sass into CSS with this command, 
`npm run build-sass` from the `/ui/` folder 

## Making content changes
The `/ui/src/pages/` folder will have all the text content for the page. 
  
## Making component changes
The components like the navigation menu, the header and the footer are located in the `/ui/src/components` folder
