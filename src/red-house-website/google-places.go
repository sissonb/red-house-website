package main

import (
	"net/http"
	"strings"
	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/memcache"
	"google.golang.org/appengine/urlfetch"
	"github.com/tdewolff/minify/json"
	"github.com/tdewolff/minify"
	"bytes"
	"time"
)

const placesMemcacheKey = "google-places"

type PlacesInfo struct {
	ApiKey  string
	PlaceId string
}

func getGooglePlaceInfo() http.Handler {
	fn := func(w http.ResponseWriter, req *http.Request) {
		ctx := appengine.NewContext(req)
		m := minify.New()
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
		getUrlMemcacheItem, err := memcache.Get(ctx, placesMemcacheKey)
		if err != nil && err != memcache.ErrCacheMiss {
			log.Errorf(ctx, "Memcache Get: %v", err)
		}
		if err == nil {
			json.Minify(m, w, strings.NewReader(string(getUrlMemcacheItem.Value)), nil)
			log.Debugf(ctx, "Memcache Hit")
			return
		}

		k := datastore.NewKey(ctx, "PlacesInfo", "red-house", 0, nil)
		gpi := new(PlacesInfo)
		if err := datastore.Get(ctx, k, gpi); err != nil {
			http.Error(w, err.Error(), 500)
			log.Errorf(ctx, "Datastore Get: %v", err)
			return
		}

		client := urlfetch.Client(ctx)
		resp, err := client.Get(
			"https://maps.googleapis.com/maps/api/place/details/json?placeid=" + gpi.PlaceId + "&key=" + gpi.ApiKey)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Errorf(ctx, "URL Fetch: %v", err)
			return
		}
		buf := new(bytes.Buffer)
		buf.ReadFrom(resp.Body)
		urlFetchResponse := buf.String()
		json.Minify(m, w, strings.NewReader(urlFetchResponse), nil)
		saveURLMemcacheItem := &memcache.Item{
			Key:        placesMemcacheKey,
			Value:      []byte(urlFetchResponse),
			Expiration: time.Second * 43200, // 12 hours
		}
		if err := memcache.Set(ctx, saveURLMemcacheItem); err != nil {
			log.Errorf(ctx, "Memcache Set: %v", err)
			return
		}
		log.Debugf(ctx, "Memcache Miss")
	}
	return http.HandlerFunc(fn)
}

func init() {
	http.Handle("/services/google-places-info/", getGooglePlaceInfo())
}
