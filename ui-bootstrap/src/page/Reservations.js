import React, { Component } from 'react';

class Reservations extends Component {
    render() {
        return (
            <section id="ReservationsPage">
                <h2>Reservations</h2>
                <div className="text-center">
                    <a href="https://www.opentable.com/r/red-house-reservations-renton?restref=61378&lang=en">
                        <img className="open-table" src="/img/open-table.png" alt="opentable icon"/>
                        <p>Reserve online using OpenTable</p>
                    </a>
                </div>
                <p className="text-right">*For parties over 10 please give us a call at <a href="tel:4252262666">(425) 226-2666</a></p>
                <p className="text-right">*We do not hold patio tables for reservations, but we do give priority to requests</p>
            </section>
        );
    }
}
export default Reservations;
