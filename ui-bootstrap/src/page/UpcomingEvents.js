import React, { Component } from 'react';

class UpcomingEvents extends Component {
    render() {
        return (
            <section id="UpcomingEventsPage">
                <h2>Upcoming Events</h2>
                <p>
                    <a href="https://calendar.google.com/calendar/embed?src=m003qrtj4371l8q0pm6u9ird7c%40group.calendar.google.com&ctz=America/Los_Angeles">
                        Open Calendar
                    </a>
                </p>
                <div className="iframe-container">
                    <iframe title="Red House Upcoming Events Calendar"
                            src="https://calendar.google.com/calendar/embed?mode=AGENDA&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=m003qrtj4371l8q0pm6u9ird7c%40group.calendar.google.com&amp;color=%23711616&amp;ctz=America%2FLos_Angeles"
                            className="iframe" frameBorder="0" scrolling="no"/>
                </div>
            </section>
        );
    }
}

export default UpcomingEvents;
