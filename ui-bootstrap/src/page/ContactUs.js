import React, {Component} from 'react';

// import pageTracking from '../../utility/pageTracking';

class ContactUs extends Component {
    componentDidMount() {
        // pageTracking('ContactUs');
    }

    render() {
        return (
            <section id="ContactUsPage">
                <h2>Contact Us</h2>
                <p>Have a question, comment, or need more info about large party's?
                    We would love to hear from you!</p>
                <p>Call us: <a href="tel:4252262666">(425) 226-2666</a></p>
                <p>Email us: <a style={{
                    wordBreak:"break-all"
                }} href="mailto:contactus@redhouserenton.com">contactus@redhouserenton.com</a>
                </p>
                <p className="text-center"><img alt="Red House Logo" src="/img/red-house-logo.png"/></p>
            </section>
        );
    }
}

export default ContactUs;
