import React from 'react';
import axios from 'axios';

class Footer extends React.Component {
    constructor() {
        super();
        this.state = {
            data: {
                result: {
                    opening_hours: {
                        open_now: '',
                        weekday_text: [],
                    },
                },
            },
        };
    }

    componentDidMount() {
        const that = this;
        axios.get('/services/google-places-info/')
            .then(function (response) {
                that.setState(response.data);
            })
            .catch(function (error) {
                const data = {"html_attributions":[],"result":{"address_components":[{"long_name":"410","short_name":"410","types":["street_number"]},{"long_name":"Burnett Avenue South","short_name":"Burnett Ave S","types":["route"]},{"long_name":"Downtown Renton","short_name":"DTR","types":["neighborhood","political"]},{"long_name":"Renton","short_name":"Renton","types":["locality","political"]},{"long_name":"King County","short_name":"King County","types":["administrative_area_level_2","political"]},{"long_name":"Washington","short_name":"WA","types":["administrative_area_level_1","political"]},{"long_name":"United States","short_name":"US","types":["country","political"]},{"long_name":"98057","short_name":"98057","types":["postal_code"]},{"long_name":"7501","short_name":"7501","types":["postal_code_suffix"]}],"adr_address":"\u003cspan class=\"street-address\"\u003e410 Burnett Ave S\u003c/span\u003e, \u003cspan class=\"locality\"\u003eRenton\u003c/span\u003e, \u003cspan class=\"region\"\u003eWA\u003c/span\u003e \u003cspan class=\"postal-code\"\u003e98057-7501\u003c/span\u003e, \u003cspan class=\"country-name\"\u003eUSA\u003c/span\u003e","formatted_address":"410 Burnett Ave S, Renton, WA 98057, USA","formatted_phone_number":"(425) 226-2666","geometry":{"location":{"lat":47.47775009999999,"lng":-122.2075897},"viewport":{"northeast":{"lat":47.47910173029149,"lng":-122.2063665697085},"southwest":{"lat":47.47640376970849,"lng":-122.2090645302915}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png","id":"5be7872aca09ac2299e7f2dba37121db94d22793","international_phone_number":"+1 425-226-2666","name":"Red House Beer & Wine Shoppe","opening_hours":{"open_now":false,"periods":[{"close":{"day":0,"time":"2100"},"open":{"day":0,"time":"1100"}},{"close":{"day":1,"time":"2100"},"open":{"day":1,"time":"1100"}},{"close":{"day":2,"time":"2100"},"open":{"day":2,"time":"1100"}},{"close":{"day":3,"time":"2100"},"open":{"day":3,"time":"1100"}},{"close":{"day":4,"time":"2200"},"open":{"day":4,"time":"1100"}},{"close":{"day":5,"time":"2200"},"open":{"day":5,"time":"1100"}},{"close":{"day":6,"time":"2200"},"open":{"day":6,"time":"1100"}}],"weekday_text":["Monday: 11:00 AM – 9:00 PM","Tuesday: 11:00 AM – 9:00 PM","Wednesday: 11:00 AM – 9:00 PM","Thursday: 11:00 AM – 10:00 PM","Friday: 11:00 AM – 10:00 PM","Saturday: 11:00 AM – 10:00 PM","Sunday: 11:00 AM – 9:00 PM"]},"photos":[{"height":2988,"html_attributions":["\u003ca href=\"https://maps.google.com/maps/contrib/104763639736725633505/photos\"\u003eDmitriy Todd\u003c/a\u003e"],"photo_reference":"CmRaAAAAjdaWvTReCJLoISG767uC2dwBdWB7GYZkNW-0KJi570kM-aDQtyM6Qbg-Zhwr-BrMTd53dQMASntIvmUqP2E240auAmis5yXeIb88DfDPabB-D-BhtSVS7MndDCwNkVL8EhCUVJf-5M5XPJp8dBQcbfN-GhTLT2ZDc0ZALMbrvLxIETN5bBylfw","width":5312},{"height":2448,"html_attributions":["\u003ca href=\"https://maps.google.com/maps/contrib/114142360400778198307/photos\"\u003eJeff Janousek\u003c/a\u003e"],"photo_reference":"CmRaAAAARrlezL7wpjgO6c_jbpvS94pmkilTtlC0UtgiiUaRPWf-x-3QQ1wl7gsXqXlMOi_adpLA7IrJbtIps6w8PLU4PhhtsAIDunpKxlfjeYR8GjDAW2DO5wbAd7shUxFF7mOsEhCp90LUXZaDHfpn1OWo7ddWGhTSMtK0SXx5HPE0KAZNpk2xJU2fAA","width":3264},{"height":2080,"html_attributions":["\u003ca href=\"https://maps.google.com/maps/contrib/115708312417142582020/photos\"\u003eCasey Thompson\u003c/a\u003e"],"photo_reference":"CmRaAAAA-xCYLQdt9cYEwLBCYtystGCiDOfgO7_daQ4lVVZVwvTogUUr9L0_HHVZjCCqHabslJXme1DuQ3cL_HH6R8aAzTCsMpqPVib2Zq4IhY1hP227scD0motxcjzf_oVK3A2IEhDgx3wUMWr1u6Mxez8xSJkWGhROGJnk3ZUq0Q3AHSglEZ_pu2WYHg","width":4160},{"height":3024,"html_attributions":["\u003ca href=\"https://maps.google.com/maps/contrib/118113922780978384911/photos\"\u003eRed House Beer &amp; Wine Shoppe\u003c/a\u003e"],"photo_reference":"CmRaAAAA5TmeSwbfil1SlppU5vnmdkdkaupbwk0GV8GMB6bbT1rhPjrbSUbdFa0OZoZ7Nz_iBIqtJQ76ylOBEZyDprZfeGZNHiq9pP0md358Yn4AK4Wi3o6UzuW5TloUZswhPWzUEhAK9clgFyz-VIhxlsm53rPsGhTux2qKvc2SptjmyIZxTF1-D-aYXA","width":4032},{"height":4008,"html_attributions":["\u003ca href=\"https://maps.google.com/maps/contrib/109706426609555550104/photos\"\u003eEd Miller\u003c/a\u003e"],"photo_reference":"CmRaAAAAipbgCAp4KrOQmhLH8GoFXB2TGA-p7i39kJcdhIEctRNcY2lAghqkc6m7wprnSAI8UwikVRPetRw6qemglrA5Toq2Zj74tF6mDEuBALnDuJTNK8Jjl1gG_i3PRg6Tpl5hEhCk0wnlWhiQFj8x2G_Ud69GGhR9Nz5g3DbgpOEXAQ8NtK6PL4UOGQ","width":5344},{"height":3036,"html_attributions":["\u003ca href=\"https://maps.google.com/maps/contrib/102388403059597453971/photos\"\u003eDouglas Taylor\u003c/a\u003e"],"photo_reference":"CmRaAAAA_okYS8hmRa8EYmg8AGFEZwl8Z9AWkg9YuTIoFr1BTJDGMJuouDc_B1FSwfv-AQ9E-G_CZT4AAHtM4RZ2y9_xEHghoqkUBA0UUgQr48pSZ8cvg_Glwlqg2gWsnq9jhk6BEhAXP1kwXHx0-UlaFvU3q4FGGhTBhQiu-7gTV6zlskvlSU0D9EKQgg","width":4048},{"height":4352,"html_attributions":["\u003ca href=\"https://maps.google.com/maps/contrib/106536197678259425079/photos\"\u003eBill Weber\u003c/a\u003e"],"photo_reference":"CmRaAAAAcGIaTQNxPwK_mkpbEMDj-LhWBBZNiAPqaicwnl2l3PXTh72_k5lgyhVKdqPfvyIo5rFY0RxetvNxC1ClzhCn7gdiz7d4nG6sjNEUYfvKUGuLNHfq6gvAQYdknWdJf_DMEhBodSx6oHbcnxl7OXcz9AelGhQjv761xV4OtWYbmM0mjbM9MynQRQ","width":3264},{"height":2268,"html_attributions":["\u003ca href=\"https://maps.google.com/maps/contrib/104057305626567659134/photos\"\u003eSarah O\u003c/a\u003e"],"photo_reference":"CmRaAAAAIyk2pHGQYoq5moz8lIwIsLL2lqNGUuCy68Atcl6SIrA1I-IyI2yyc5JtHpQPo1usrIdUdspqq4L_izmP5e67TWrCQzukMNdZUIwG23HubheeRptF2RhmgaiQf3LSjtAkEhA_RAQJKMMZZf-YjiYEH0pDGhQfhEHHYd9SUzKBbmIsTSvNxozDuw","width":4032},{"height":2268,"html_attributions":["\u003ca href=\"https://maps.google.com/maps/contrib/104057305626567659134/photos\"\u003eSarah O\u003c/a\u003e"],"photo_reference":"CmRaAAAAvcJvAoGPcrlBcG8Y3gUOdhakyfCUMQ_zB1tUc042DsggkZFFEsl4zsv-wCpK8pr9GJjcY7WxYJ0kulGypyC88MetM3W2EW1C-OL3tEPQj8FnW1pcbGX_cRmon16-Zk7SEhAlMrC8yyYMArHPOWrqdtumGhRn-cm2KshrCa0v5A__Fb2Uvp7qQw","width":4032},{"height":3120,"html_attributions":["\u003ca href=\"https://maps.google.com/maps/contrib/111253366281338190509/photos\"\u003eRyo Canda\u003c/a\u003e"],"photo_reference":"CmRaAAAAd8AgXC3ALAobgmzB4FmyS725m8F-i4Go-NeRxc4RoYiZn_-zOoxqDd11HoxudORzhjIvpIdrGXHfWgjkt5qdRDfUI18TUnGBroEro7Phvird35Gg2xFRS6heDzeiqWJSEhCWRYAWpv13L0iNI3Lacl1yGhS4bqAV_kU0O3DyK7Oh49P-deRFAw","width":4160}],"place_id":"ChIJ0f3UTVNdkFQR-qomtr-RMDw","plus_code":{"compound_code":"FQHR+4X Renton, Washington, United States","global_code":"84VVFQHR+4X"},"price_level":2,"rating":4.4,"reference":"CmRRAAAAzeQpf7XLXPwNh-I_NC_dNT17rXOvVdaWJfAopuH8qddhvVvCcwMODdsVWyh4shCVSZYcRrZ2tcZwYHHRD1Oaesj0ls4rhsULG-v53dODRBr4A-xE1_ta7g-eopBFoWSMEhCjjWFIWIfCyGY-gSjvdTaHGhTPHi56SLEn7W-j0LnkodCKXgA6EA","reviews":[{"author_name":"John","author_url":"https://www.google.com/maps/contrib/115215814110553755482/reviews","language":"en","profile_photo_url":"https://lh5.googleusercontent.com/-NLEPuG_GWvQ/AAAAAAAAAAI/AAAAAAAAEF4/BkWQVZAmMVk/s128-c0x00000000-cc-rp-mo-ba3/photo.jpg","rating":5,"relative_time_description":"a month ago","text":"Everything is delicious. And they usually have amazing specials. Great beer and wine list, plus the staff is beyond friendly. It feels incredibly homey and comfortable. The top floor and outside patio seem well situated for private events. Best restaurant in Renton.","time":1531331684},{"author_name":"Julia C.","author_url":"https://www.google.com/maps/contrib/100593944820838273346/reviews","language":"en","profile_photo_url":"https://lh6.googleusercontent.com/-xvBuCMwO5Eo/AAAAAAAAAAI/AAAAAAAAG_0/jQen_9LuDec/s128-c0x00000000-cc-rp-mo-ba5/photo.jpg","rating":5,"relative_time_description":"a month ago","text":"If I could write a review for everything single menu item we ordered each time we've gone, I would. Their food is amazing. The atmosphere is adorable and everyone from managers to wait staff are nice and attentive. They have tapas and entrées. Their portions are large and you could make a meal solely out of the size of some of the tapas. We've gotten the nightly specials and never once been disappointed. If they are crowded you may have to walk a block or two from your car but you'll need the extra walk at the end of the meal because you'll be so stuffed.","time":1531405777},{"author_name":"Joseph Paolini","author_url":"https://www.google.com/maps/contrib/109064536037720338267/reviews","language":"en","profile_photo_url":"https://lh4.googleusercontent.com/-cKOOLQXiehY/AAAAAAAAAAI/AAAAAAAAA0I/1zjQbMO3rwU/s128-c0x00000000-cc-rp-mo-ba4/photo.jpg","rating":5,"relative_time_description":"a month ago","text":"Loved the atmosphere, drinks, service, and the food. Some of the main course portions can be a little small, so ask which plates are hearty if you are hungry. Or grab some tapas with your main, all the ones we shared we're good.","time":1531523063},{"author_name":"Kay Schuetz","author_url":"https://www.google.com/maps/contrib/101713550914791862767/reviews","language":"en","profile_photo_url":"https://lh5.googleusercontent.com/-qk1Sza4HPec/AAAAAAAAAAI/AAAAAAAAAAA/APUIFaO7k78g3iYmyVW_n9H4SJqVf6790Q/s128-c0x00000000-cc-rp-mo/photo.jpg","rating":5,"relative_time_description":"a month ago","text":"We love our evenings here. We enjoy ourselves so much that we almost always get dessert- which is something we rarely do elsewhere. Whether we go for the cheese plate or have dinner, we are always happy. We so enjoy the staff and we look forward to our next visit.","time":1529377407},{"author_name":"Beef Cake","author_url":"https://www.google.com/maps/contrib/117110717165415521481/reviews","language":"en","profile_photo_url":"https://lh3.googleusercontent.com/-V8O6YHSlxag/AAAAAAAAAAI/AAAAAAAAAAA/APUIFaNIKJsIZsg4EKVufjBo7h0_EXtUqw/s128-c0x00000000-cc-rp-mo/photo.jpg","rating":5,"relative_time_description":"a month ago","text":"The best restaurant in Renton by far. The service was impeccable. The food was great, especially for this area. Everything was made from scratch, even the ketchup and tartar sauce. Totally worth the five stars.","time":1529866529}],"scope":"GOOGLE","types":["restaurant","food","point_of_interest","establishment"],"url":"https://maps.google.com/?cid=4337126693738556154","utc_offset":-420,"vicinity":"410 Burnett Avenue South, Renton","website":"http://www.redhousebeerandwine.com/"},"status":"OK"};
                that.setState({
                    data
                });
            });
    }

    render() {
        const weekdayTextIndex = (new Date().getDay() + 6) % 7;
        return (
            <footer id="Footer" className="container-fluid">
                <div className="row">
                    <div className="col">
                        <h3>Call Us</h3>
                        <p className="call-us">
                            <a href="tel:4252262666">(425) 226-2666</a>
                        </p>
                        <h3>Email Us</h3>
                        <p className="email-us">
                            <a style={{
                                wordBreak: "break-word"
                            }} href="mailto:contactus@redhouserenton.com">contactus@redhouserenton.com</a>
                        </p>
                    </div>
                    <div className="col">
                        <h3>Directions</h3>
                        <address>
                            <p className="directions">
                                <a href="https://www.google.com/maps/dir/My+Location/Red+House+Beer+%26+Wine+Shoppe,+410+Burnett+Ave+S,+Renton,+WA+98057">
                                    410 Burnett Ave S, Renton, WA 98057
                                </a>
                            </p>
                        </address>
                        <h3>Menu</h3>
                        <p className="menu">
                            <a href="https://drive.google.com/uc?authuser=0&id=0B8zWSghkNN83SUR6RE1IVUE2QWc&export=download">
                                Click here to download the menu
                            </a>
                        </p>
                    </div>
                    <div className="col">
                        <h3>Restaurant Hours</h3>
                        <ul className="restaurant-hours">
                            {this.state.data.result.opening_hours.weekday_text.map((dayHours, index) => {
                                if (weekdayTextIndex === index) {
                                    return <li key={dayHours}><b>{dayHours}</b></li>;
                                } else {
                                    return <li key={dayHours}>{dayHours}</li>;
                                }
                            })}
                        </ul>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;