import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Navigation extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <Link className="navbar-brand" to="/">The Red House</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link className="nav-link" to="/menu/">Menu</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/reservations/">Reservations</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/faq/">FAQ</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/upcoming-events/">Upcoming Events</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/contact-us/">Contact Us</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Navigation;
