import React, {Component} from 'react';
import {Route} from "react-router-dom";
import Navigation from './component/Navigation';
import Footer from './component/Footer';
import Home from './page/Home';
import Menu from './page/Menu';
import Reservations from './page/Reservations';
import FAQ from './page/FAQ';
import ContactUs from './page/ContactUs';
import UpcomingEvents from './page/UpcomingEvents';

class App extends Component {
    render() {
        return (
            <div>
                <header className="Header"/>
                <Navigation/>
                <main className="container">
                    <Route exact path="/" component={Home}/>
                    <Route path="/menu/" component={Menu}/>
                    <Route path="/reservations/" component={Reservations}/>
                    <Route path="/faq/" component={FAQ}/>
                    <Route path="/contact-us/" component={ContactUs}/>
                    <Route path="/upcoming-events/" component={UpcomingEvents}/>
                </main>
                <Footer/>
            </div>
        );
    }
}

export default App;
